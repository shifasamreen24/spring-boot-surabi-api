# spring-boot-surabi-api

- base-path: /app/** 
- swagger-path: /app/swagger-ui.html


## Register Controller ( access level user, admin, all)
- Register user 

path: /register

## Menu Controller ( access level user)
- View Menu 
- Place Order / generate bill 
- Extra endpoints to test


path: /user/** 

## User Controller ( access level admin ONLY)
- CRUD operations on users

path: /admin/** 

## Audit Controller ( access level admin ONLY)
- View daily logs
- Compute monthly sale


path: /admin/audit/** 
