package com.greatlearning.surabi.service.impl;

import com.greatlearning.surabi.entity.MenuEntity;
import com.greatlearning.surabi.model.MenuDto;
import com.greatlearning.surabi.repository.MenuRepository;
import com.greatlearning.surabi.util.TestUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@SpringBootTest
public class MenuServiceImplTest {

    @Mock
    private MenuRepository menuRepository;

    @InjectMocks
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp() throws ParseException {
        MenuDto menuDto1 = new MenuDto(123L,"item123",300.0);
        Mockito.when(menuRepository.save(TestUtil.convertMenuDtoToEntity(menuDto1))).thenReturn(TestUtil.convertMenuDtoToEntity(menuDto1));
        menuService.addAMenuItem(TestUtil.convertMenuDtoToEntity(menuDto1));

        MenuDto menuDto2 = new MenuDto(124L,"item124",400.0);
        Mockito.when(menuRepository.save(TestUtil.convertMenuDtoToEntity(menuDto2))).thenReturn(TestUtil.convertMenuDtoToEntity(menuDto2));
        menuService.addAMenuItem(TestUtil.convertMenuDtoToEntity(menuDto2));

        MenuDto menuDto3 = new MenuDto(125L,"item125",500.0);
        Mockito.when(menuRepository.save(TestUtil.convertMenuDtoToEntity(menuDto3))).thenReturn(TestUtil.convertMenuDtoToEntity(menuDto3));
        menuService.addAMenuItem(TestUtil.convertMenuDtoToEntity(menuDto3));

    }

    @Test
    public void test_get_menu_items() throws Exception {
        Mockito.when(menuRepository.findAll()).thenReturn(TestUtil.getMenuItems());
        List<MenuEntity> menuResult = menuService.getAllMenuItems();
        Assertions.assertNotNull(menuResult);
        Assertions.assertEquals(TestUtil.getMenuItems(), menuResult);
    }

    @Test
    public void test_add_menu_item() throws Exception {
        MenuDto menuDto = new MenuDto(100L,"item100",100.0);
        Mockito.when(menuRepository.save(TestUtil.convertMenuDtoToEntity(menuDto))).thenReturn(TestUtil.convertMenuDtoToEntity(menuDto));
        MenuEntity menuResult = menuService.addAMenuItemSave(TestUtil.convertMenuDtoToEntity(menuDto));
        Assertions.assertNotNull(menuResult);
        Assertions.assertEquals(100.0, menuResult.getPrice());
    }

    @Test
    public void test_bill_generation(){
        Collection<Long> idsInput = new ArrayList<>();
        idsInput.add(124L);
        idsInput.add(125L);
        Mockito.when(menuRepository.generateBill(idsInput)).thenReturn(900.0);
    }

}
