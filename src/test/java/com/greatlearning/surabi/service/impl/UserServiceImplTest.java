package com.greatlearning.surabi.service.impl;

import com.greatlearning.surabi.entity.UserEntity;
import com.greatlearning.surabi.model.UserDto;
import com.greatlearning.surabi.repository.UserRepository;
import com.greatlearning.surabi.util.TestUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.omg.CORBA.portable.ApplicationException;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.util.List;

@SpringBootTest
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @BeforeEach
    public void setUp() throws ParseException {

        UserDto userDto1 = new UserDto(1L,"admin","pass1",true);
        Mockito.when(userRepository.save(TestUtil.convertUserDtoToEntity(userDto1))).thenReturn(TestUtil.convertUserDtoToEntity(userDto1));
        userService.addAUser(TestUtil.convertUserDtoToEntity(userDto1));

        UserDto userDto2 = new UserDto(2L,"user2","pass2",true);
        Mockito.when(userRepository.save(TestUtil.convertUserDtoToEntity(userDto2))).thenReturn(TestUtil.convertUserDtoToEntity(userDto2));
        userService.addAUser(TestUtil.convertUserDtoToEntity(userDto2));

        UserDto userDto3 = new UserDto(3L,"user3","pass3",true);
        Mockito.when(userRepository.save(TestUtil.convertUserDtoToEntity(userDto3))).thenReturn(TestUtil.convertUserDtoToEntity(userDto3));
        userService.addAUser(TestUtil.convertUserDtoToEntity(userDto3));
    }

    @Test
    public void test_add_a_user() throws Exception {
        UserDto userDto = new UserDto(100L,"user","pass",true);
        Mockito.when(userRepository.save(TestUtil.convertUserDtoToEntity(userDto))).thenReturn(TestUtil.convertUserDtoToEntity(userDto));
        userService.addAUser(TestUtil.convertUserDtoToEntity(userDto));
    }

    @Test
    public void get_all_users() throws Exception {
        Mockito.when(userRepository.findAll()).thenReturn(TestUtil.getAllUsers());
        List<UserEntity> userEntities = userService.getAllUsers();
        Assertions.assertNotNull(userEntities);
        Assertions.assertEquals(TestUtil.getAllUsers(), userEntities);
    }
}
