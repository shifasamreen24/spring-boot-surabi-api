package com.greatlearning.surabi.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.greatlearning.surabi.entity.AuditEntity;
import com.greatlearning.surabi.entity.MenuEntity;
import com.greatlearning.surabi.entity.UserEntity;
import com.greatlearning.surabi.model.MenuDto;
import com.greatlearning.surabi.model.UserDto;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestUtil {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static byte[] convertObjectToJson(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    public static MenuEntity convertMenuDtoToEntity(MenuDto menuDto) throws ParseException {
        MenuEntity menuEntity = new MenuEntity();
        menuEntity.setId(menuDto.getId());
        menuEntity.setPrice(menuDto.getPrice());
        menuEntity.setItem(menuDto.getItem());
        return menuEntity;
    }

    public static List<MenuEntity> getMenuItems() throws ParseException{
        List<MenuEntity> menu = new ArrayList<>();
        MenuDto menuDto1 = new MenuDto(123L,"item123",300.0);
        MenuDto menuDto2 = new MenuDto(124L,"item124",400.0);
        MenuDto menuDto3 = new MenuDto(125L,"item125",500.0);
        menu.add(TestUtil.convertMenuDtoToEntity(menuDto1));
        menu.add(TestUtil.convertMenuDtoToEntity(menuDto2));
        menu.add(TestUtil.convertMenuDtoToEntity(menuDto3));
        return menu;
    }

    public static UserEntity convertUserDtoToEntity(UserDto userDto) throws ParseException {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userDto.getId());
        userEntity.setUsername(userDto.getUsername());
        userEntity.setPassword(userDto.getPassword());
        userEntity.setEnabled(userDto.getEnabled());
        return userEntity;
    }

    public static List<UserEntity> getAllUsers() throws ParseException{
        List<UserEntity> users = new ArrayList<>();
        UserDto userDto1 = new UserDto(1L,"admin","pass1",true);
        UserDto userDto2 = new UserDto(2L,"user2","pass2",true);
        UserDto userDto3 = new UserDto(3L,"user3","pass3",true);
        users.add(TestUtil.convertUserDtoToEntity(userDto1));
        users.add(TestUtil.convertUserDtoToEntity(userDto2));
        users.add(TestUtil.convertUserDtoToEntity(userDto2));
        return users;
    }

    public static List<AuditEntity> getAuditLogs(){

        List<AuditEntity> auditLogs = new ArrayList<>();
        return auditLogs;

    }

}
