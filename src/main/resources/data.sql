-- create table authorities (
--                              username varchar(50) not null,
--                              authority varchar(50) not null,
--                              foreign key (username) references user_entity (username)
-- );

ALTER TABLE `authorities`
    ADD CONSTRAINT `fk_authorities`
        FOREIGN KEY (`username`) REFERENCES `users` (`username`);

insert into users (id, username,password, enabled) values (1, 'admin', '$2a$10$HGohNIHAitEPdw.bZ52nFeuRRsqzWRN1f8MxpAXffTKuy5.gAkGYu', true);
insert into authorities (username, role) values ('admin', 'ROLE_ADMIN' );
insert into users (id, username,password, enabled) values (2, 'user2', '$2a$10$uFS8JeLO8yr6c9zSdZy.G.LiaGANecYXaaWj/E9FPTpDTMkh4ObjC', true);
insert into authorities (username, role) values ('user2', 'ROLE_USER' );

insert into menu_entity values(1, 'item1',100.00);
insert into menu_entity values(2, 'item2',200.00);
insert into menu_entity values(3, 'item3',300.00);

