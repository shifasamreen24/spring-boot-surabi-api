package com.greatlearning.surabi.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Date;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RestaurantCustomException extends Throwable {
    private HttpStatus code;
    private String message;
    private Date timestamp;

    public RestaurantCustomException(String message, HttpStatus code){
        this.code=code;
        this.message=message;
    }
    public RestaurantCustomException(String message, HttpStatus code, Date timestamp){
        this.code=code;
        this.message=message;
        this.timestamp=timestamp;
    }
}
