package com.greatlearning.surabi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@RestControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(value = {NullPointerException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public RestaurantCustomException resourceNotFoundException(Exception ex, WebRequest request) {
        RestaurantCustomException message = new RestaurantCustomException(
                HttpStatus.NOT_FOUND,
                ex.getMessage(),
                new Date());

        return message;
    }
}
