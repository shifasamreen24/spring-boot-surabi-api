package com.greatlearning.surabi.controller;

import com.greatlearning.surabi.entity.UserEntity;
import com.greatlearning.surabi.exception.RestaurantCustomException;
import com.greatlearning.surabi.repository.UserRepository;
import com.greatlearning.surabi.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/admin")
public class UserController{

    @Autowired
    IUserService iUserService;
    @Autowired
    UserRepository userRepository;

    //read
    @GetMapping("/users")
    @ApiOperation(value = "Get All UserEntity", notes = "Fetch all UserEntity as list")
    public List<UserEntity> getAllUsers() throws RestaurantCustomException{
        return userRepository.findAll();
    }
    @GetMapping("/user")
    @ApiOperation(value = "Get a user by id", notes = "Fetch specific user")
    public Optional<UserEntity> getAUserById(Long id) throws RestaurantCustomException{
        return userRepository.findById(id);
    }

    //add
    @PostMapping("/user")
    @ApiOperation(value = "Add a new user", notes = "Add a specific user")
    public void addAUser(UserEntity userEntity) throws RestaurantCustomException{
        iUserService.addAUser(userEntity);
    }
    @PostMapping("/users")
    @ApiOperation(value = "Add a list of users", notes = "Add users in bulk")
    public void addAllUser(@RequestBody List<UserEntity> userEntities) throws RestaurantCustomException{
        iUserService.addAllUser(userEntities);
    }

    //delete
    @DeleteMapping("/user")
    @ApiOperation(value = "Delete a user", notes = "Remove specific user by id")
    public void deleteUserById(Long id) throws RestaurantCustomException{
        iUserService.deleteUserById(id);
    }
    @DeleteMapping("/users")
    @ApiOperation(value = "Prune Table", notes = "Remove all records from user table")
    public void deleteAllUsers() throws RestaurantCustomException{
        iUserService.deleteAllUsers();
    }

    //update
    @PutMapping("/user")
    public void updateUserNameById(String userName, Long id) throws RestaurantCustomException {
        Optional<UserEntity> currentUser =  userRepository.findById(id);
        if(currentUser.isPresent()) {
            UserEntity user = currentUser.get();
            user.setUsername(userName);
            userRepository.saveAndFlush(user);
        }
        else {
            throw new RestaurantCustomException("User does not exist for given id ", HttpStatus.NOT_FOUND);
        }
    }

}
