package com.greatlearning.surabi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/")
    public String homeDisplay(){
        return "<h1>Login Successful</h1>";
    }
}
