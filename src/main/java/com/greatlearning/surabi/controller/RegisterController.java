package com.greatlearning.surabi.controller;

import com.greatlearning.surabi.entity.AuthoritiesEntity;
import com.greatlearning.surabi.entity.UserEntity;
import com.greatlearning.surabi.model.UserDto;
import com.greatlearning.surabi.repository.AuthoritiesRepository;
import com.greatlearning.surabi.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Slf4j
@Controller
public class RegisterController {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthoritiesRepository authoritiesRepository;

    @GetMapping(value = "/register")
    public ModelAndView register(ModelAndView model, UserDto userDto) {
        model.setViewName("register");
        model.addObject("userDto", userDto);
        return model;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister( ModelAndView modelAndView, UserDto userDto) {

        log.info("Inside controller");
        UserEntity userEntity = new UserEntity();
        userEntity.setEnabled(Boolean.TRUE);
        userEntity.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userEntity.setUsername(userDto.getUsername());

        AuthoritiesEntity authoritiesEntity = new AuthoritiesEntity();
        authoritiesEntity.setUsername(userEntity.getUsername());
        authoritiesEntity.setRole("ROLE_USER");
        userRepository.saveAndFlush(userEntity);
        authoritiesRepository.saveAndFlush(authoritiesEntity);
        log.info("Save successful");

        return "redirect:/login";
    }
}
