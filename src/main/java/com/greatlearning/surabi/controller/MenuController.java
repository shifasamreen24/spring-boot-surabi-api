package com.greatlearning.surabi.controller;

import com.greatlearning.surabi.entity.MenuEntity;
import com.greatlearning.surabi.repository.MenuRepository;
import com.greatlearning.surabi.service.IMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/user")
public class MenuController {

    @Autowired
    IMenuService iMenuService;

    @Autowired
    MenuRepository menuRepository;

    //read
    @GetMapping(value = "/menu")
    @ApiOperation(value = "Get All Menu items", notes = "Fetch all items from menu as list")
    public List<MenuEntity> getAllMenuItems(){
        return menuRepository.findAll();
    }

    //select menu and generate bill
    @PostMapping("/placeOrder")
    @ApiOperation(value = "Select from Menu", notes = "Fetch total bill amount")
    public Map<String, Double> generateBill(@RequestBody Collection<Long> ids){
        Double total_bill_amount= menuRepository.generateBill(ids);
        HashMap<String, Double> map = new HashMap<>();
        map.put("Bill Generated",total_bill_amount);
        return map;
    }

    //add
    @PostMapping("/menu_item")
    @ApiOperation(value = "Add a new menu item", notes = "Add a specific item")
    public MenuEntity addAMenuItem(MenuEntity menuEntity){
        iMenuService.addAMenuItem(menuEntity);
        return menuEntity;
    }
    @PostMapping("/menu_items")
    @ApiOperation(value = "Add a list of menu items", notes = "Add items in bulk")
    public void addAllMenuItems(@RequestBody List<MenuEntity> menuEntities){
        iMenuService.addAllMenuItems(menuEntities);
    }

    //delete
    @DeleteMapping("/menu_item")
    @ApiOperation(value = "Delete a menu item", notes = "Remove specific item by id")
    public void deleteMenuItemById(Long id){
        iMenuService.deleteMenuItemById(id);
    }
    @DeleteMapping("/menu_items")
    @ApiOperation(value = "Prune Table", notes = "Remove all records from items menu")
    public void deleteAllMenuItems(){
        iMenuService.deleteAllMenuItems();
    }
}
