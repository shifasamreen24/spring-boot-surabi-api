package com.greatlearning.surabi.controller;

import com.greatlearning.surabi.entity.AuditEntity;
import com.greatlearning.surabi.repository.AuditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/audit")
public class AuditController {

    @Autowired
    AuditRepository auditRepository;

    @GetMapping("/logs")
    public List<AuditEntity> getAllOrdersLogs(){
        return auditRepository.findAll();
    }
    @GetMapping("/sale_this_month")
    public Double getTotalSalesThisMonth(){
        return auditRepository.totalSalesThisMonth();
    }
}
