package com.greatlearning.surabi.repository;

import com.greatlearning.surabi.entity.AuditEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditRepository extends JpaRepository<AuditEntity, Long> {
    @Query( value = "SELECT SUM(sale) FROM (SELECT m.bill_generated as sale FROM AUDIT_ENTITY m WHERE month(m.creation_date) = month(current_date()))", nativeQuery = true)
    Double totalSalesThisMonth();
}
