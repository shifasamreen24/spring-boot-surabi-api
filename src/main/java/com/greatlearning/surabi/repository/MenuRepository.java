package com.greatlearning.surabi.repository;

import com.greatlearning.surabi.entity.MenuEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface MenuRepository extends JpaRepository<MenuEntity, Long> {
    @Query( value = "SELECT SUM(bill) FROM (SELECT m.id, m.price as bill FROM MENU_ENTITY m WHERE m.id IN :ids)", nativeQuery = true)
    Double generateBill(@Param("ids") Collection<Long> ids);
}
