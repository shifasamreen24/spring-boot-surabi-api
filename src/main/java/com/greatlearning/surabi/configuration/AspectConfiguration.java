package com.greatlearning.surabi.configuration;

import com.greatlearning.surabi.entity.AuditEntity;
import com.greatlearning.surabi.repository.AuditRepository;
import com.greatlearning.surabi.repository.MenuRepository;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;
import java.util.Date;

@Slf4j
@Aspect
@Configuration
public class AspectConfiguration {

    @Autowired
    AuditRepository auditRepository;

    @Autowired
    MenuRepository menuRepository;

    @Around("execution(public * com.greatlearning.surabi.service.impl.*.*(..) )")
    public void logBeforeAndAfterAllMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        log.info(proceedingJoinPoint.getSignature().getName()+ " Started");
        proceedingJoinPoint.proceed();
        log.info(proceedingJoinPoint.getSignature().getName()+ " Ended");

    }
    @AfterReturning(pointcut = "execution(public * com.greatlearning.surabi.repository.MenuRepository.generateBill(..) )",
            returning="returnValue")
    public void logBeforeAddMenuItem(JoinPoint joinPoint, Double returnValue){
        Collection<Long> ids=  (Collection<Long>)joinPoint.getArgs()[0];
        log.info("Items ordered are "+ ids);
        auditRepository.saveAndFlush(AuditEntity.builder()
                .creationDate(new Date())
                .description("details of menu item added "+ joinPoint.getArgs()[0])
                .orderedItems(ids.toString())
                .billGenerated(returnValue)
                .build());
    }
}
