package com.greatlearning.surabi.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

    @Id
    @Column(unique=true)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @NonNull
    @Column(unique=true)
    private String username;
    @NonNull
    @Column
    private String password;
    @NonNull
    @Column
    private Boolean enabled;

}
