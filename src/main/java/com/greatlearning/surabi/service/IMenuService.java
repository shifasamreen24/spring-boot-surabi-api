package com.greatlearning.surabi.service;

import com.greatlearning.surabi.entity.MenuEntity;

import java.util.List;

public interface IMenuService {
    //read
    List<MenuEntity> getAllMenuItems();
    //add
    void addAMenuItem(MenuEntity menuEntity);
    void addAllMenuItems(List<MenuEntity> menuEntities);
    //delete
    void deleteAllMenuItems();
    void deleteMenuItemById(Long id);
}
