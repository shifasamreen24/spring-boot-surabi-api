package com.greatlearning.surabi.service;

import com.greatlearning.surabi.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    //read
    List<UserEntity> getAllUsers();
    Optional<UserEntity> getAUserById(Long id);
    //add
    void addAUser(UserEntity userEntity);
    void addAllUser(List<UserEntity> userEntities);
    //delete
    void deleteAllUsers();
    void deleteUserById(Long id);

}
