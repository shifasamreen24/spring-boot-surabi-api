package com.greatlearning.surabi.service.impl;

import com.greatlearning.surabi.entity.UserEntity;
import com.greatlearning.surabi.repository.UserRepository;
import com.greatlearning.surabi.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;

    // read methods
    @Override
    public List<UserEntity> getAllUsers(){
        return userRepository.findAll();
    }
    @Override
    public Optional<UserEntity> getAUserById(Long id){
        return userRepository.findById(id);
    }

    //add user
    @Override
    public void addAUser(UserEntity userEntity){
        userRepository.saveAndFlush(userEntity);
    }
    @Override
    public void addAllUser(List<UserEntity> userEntities){
        userRepository.saveAll(userEntities);
        userRepository.flush();
    }
    //delete user
    @Override
    public void deleteUserById(Long id){
        userRepository.deleteById(id);
    }
    @Override
    public void deleteAllUsers(){
        userRepository.deleteAll();
    }


}
