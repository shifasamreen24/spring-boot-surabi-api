package com.greatlearning.surabi.service.impl;

import com.greatlearning.surabi.entity.MenuEntity;
import com.greatlearning.surabi.repository.MenuRepository;
import com.greatlearning.surabi.service.IMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Slf4j
@Service
public class MenuServiceImpl implements IMenuService {

    @Autowired
    MenuRepository menuRepository;

    @Override
    public List<MenuEntity> getAllMenuItems() {
        log.info(menuRepository.findAll().toString());
        return menuRepository.findAll();
    }

    @Override
    public void addAMenuItem(MenuEntity menuEntity) {
        menuRepository.saveAndFlush(menuEntity);
    }

    @Override
    public void addAllMenuItems(List<MenuEntity> menuEntities) {
        menuRepository.saveAll(menuEntities);
        menuRepository.flush();
    }

    @Override
    public void deleteAllMenuItems() {
        menuRepository.deleteAll();
    }

    @Override
    public void deleteMenuItemById(Long id) {
        menuRepository.deleteById(id);
    }
}
